import boto3
import urllib
import urllib.parse
TARGET_BUCKET = 'mynews3bucket05222023'

def lambda_handler(event, context):
    
    # Get incoming bucket and key
    source_bucket = event['Records'][0]['s3']['bucket']['name']
    #source_key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'])
    source_key =  urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    obj_name = source_key.split('/')[-1]
    print('obj_name:', obj_name)

    # Copy object to different bucket
    s3_resource = boto3.resource('s3')
    copy_source = {
        'Bucket': source_bucket,
        'Key': source_key
    }
    target_key = source_key # Change if desired
    #target_key = ""

    s3_resource.Bucket(TARGET_BUCKET).Object(target_key).copy(copy_source, ExtraArgs={'ACL': 'bucket-owner-full-control'})